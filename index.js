const { app, BrowserWindow, ipcMain } = require('electron')
//const PrintFun = require('./src/components/print.jsx');


// var fs = require('fs')
// var path = require('path')

// module.exports = path.join(__dirname, fs.readFileSync(path.join(__dirname, 'path.txt'), 'utf-8'))

//const CATCH_ON_MAIN = require('./src/utils/constants.js');

function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 1850,
    height: 800,
    webPreferences: {
      nativeWindowOpen: true,
      enableRemoteModule: false,
      nodeIntegration: true,
      allowRunningInsecureContent: false,
      userAgent: 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.122 Mobile Safari/537.36',
      webSecurity: true,
    
      
    }
  })
  
  // and load the index.html of the app.
  win.webContents.setUserAgent("Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.122 Mobile Safari/537.36");
  win.loadURL('http://localhost:3000/');
  console.log(win.webContents.getPrinters());
  
  
  // if(PrintFun === 'true'){
    //   win.webContents.on('did-finish-load', () => { 
      //     win.webContents.print({silent: true, deviceName: '' }, function(success){
        //       console.log(success);
        //       }); 
        //     })
        //   }
        
        
        
        ipcMain.on('catch-on-main' ,function(event, url){
          console.log('Printing doc', url);
          let win2 = new BrowserWindow({
            show: false,
            width: 1000,
            height: 2000,
            webPreferences: {
              nodeIntegration: true
            }
          })
          win2.loadURL(url);
          win2.webContents.on('did-finish-load', () => { 
            win2.webContents.print({silent: true, deviceName: '' }, function(success){
              console.log(success);
            }); 
          });
        } );
        
      }
      app.userAgentFallback = app.userAgentFallback.replace('Electron/' + process.versions.electron, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.122 Mobile Safari/537.36');
      app.whenReady().then(createWindow);