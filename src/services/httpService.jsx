import axios from "axios";

// export function setJwt(jwt) {
//  // axios.defaults.headers.common["x-auth-token"] = jwt;
// }

axios.interceptors.response.use(null, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    console.log("loggin the error", error);
   alert("an unexpected error occured");
  }

  return Promise.reject(error);
});

export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  
};
