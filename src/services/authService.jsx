import http from "./httpService";
import { apiUrl } from "../config.json";

//import jwtDecode from "jwt-decode";

const apiEndpoint = apiUrl + "/auth";
const tokenKey = "token";
const usernameKey = "username";
const roleKey = "role";

//http.setJwt(getJwt());


export async function login(identifier, password) {
 return await http.post(apiEndpoint, { identifier, password }).then(response => {
    if(response.data.message)
    return alert(response.data.message);
    localStorage.setItem(tokenKey, response.data.user.stsTokenManager.accessToken); // JWT= JSON Web Token
    localStorage.setItem(usernameKey, response.data.user.email);
    //localStorage.setItem(roleKey, response.data.user.role.name);
    
  })
}

export function loginWithJwt(data) {
  localStorage.setItem(tokenKey, data.jwt);
  localStorage.setItem(usernameKey, data.user.username);
    localStorage.setItem(roleKey, data.user.role.name);
}

export function logout() {
  localStorage.clear();
}

export function getCurrentUser() {
  try {
    const username = localStorage.getItem(usernameKey) ;
    const role = localStorage.getItem(roleKey);
    if (username === null)
    return null;
        return { username, role };
    // const user = localStorage.getItem(userKey) ;
    // return user;
   // return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}

export default {
  login,
  logout,
  getCurrentUser,
  loginWithJwt
};
