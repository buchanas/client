import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/Bottles";


export function getBottles() {
  return http.get(apiEndpoint);
}

// export function getBottle(bottleId) {
//     return http.get(`${apiEndpoint}/${bottleId}`);
//   }
