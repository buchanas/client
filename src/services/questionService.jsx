import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/Questions";
const customHeader = { 'Authorization': "Bearer " + localStorage.getItem("token") };
function QuestionUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getQuestions() {
  return http.get(apiEndpoint);
}

export function saveQuestion(Question) {
  if (Question.id) {
    const body = { ...Question};
    delete body.id;
    return http.put(QuestionUrl(Question.id), body
    ,{
        headers: customHeader
      }
    
    );
  } 
}

export function getQuestion(QuestionId) {
  return http.get(QuestionUrl(QuestionId));
}


// export function deleteQuestion(QuestionId) {
//  return http.delete(QuestionUrl(QuestionId), {headers: customHeader});
 
// }
