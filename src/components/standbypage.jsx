import React, { Component } from 'react';
import Logo from './logo';
import Description from './description';
import {Grid, Box, withStyles, Button, Select, MenuItem} from '@material-ui/core';
import { Link } from 'react-router-dom';

const style = theme => ({
    description: {
        height: 200,
    },
    date:{ 
        fontSize: 20,
        width: '10%',
        margin: '10px'
    }
})

class StandByPage extends Component {
    state = { 
       
     }

     renderDay = () => {
         const data = [];
         for (let i = 1; i < 32; i++) {
            data.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
         }
         return data;
     }
    renderYear = () => {
        const data = [];
        for (let i = 2002; i > 1959; i--) {
           data.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
        }
        return data;
    }
    
    

    
    render() { 
        const {classes} = this.props;

        
        return (  
            <React.Fragment>
            <Box component='div' m={5}>
            <Grid container >
                <Grid item xs={null} sm={1} />
                <Grid item xs={12} sm={10} >
                    <Logo />
                </Grid>
                <Grid item xs={null} sm={1} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <Grid container alignItems='center' className={classes.description} >
                <Grid item xs={null} sm={3} />
                <Grid  item xs={12} sm={6} >
                    <Description />
                </Grid>
                <Grid item xs={null} sm={3} />
            </Grid>
            </Box>
            <center>
                <h3>
                    Please Set Your Date of Birth to start
                </h3>
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' className={classes.description} >
                <Grid item xs={null} sm={3} />
                <Grid item xs={12} sm={6}  >
                <center>
                
                <Select
                    labelId="day"
                    id="demo-simple-select-placeholder-label"
                    displayEmpty
                    className={classes.date}
                    value={this.props.values.day}
                    onChange={this.props.handleChange('day')}
                    >
                        <MenuItem value="" disabled>
                            Day
                        </MenuItem>
                     {this.renderDay()}
                </Select>
                <Select
                    labelId="month"
                    id="demo-simple-select-placeholder-label"
                    displayEmpty
                    className={classes.date}
                    value={this.props.values.month}
                    onChange={this.props.handleChange('month')}
                    >
                     <MenuItem value="" disabled>
                        Month
                     </MenuItem>
                     <MenuItem value='1'>Jan</MenuItem>
                     <MenuItem value='2'>Feb</MenuItem>
                     <MenuItem value='3'>Mar</MenuItem>
                     <MenuItem value='4'>Apr</MenuItem>
                     <MenuItem value='5'>May</MenuItem>
                     <MenuItem value='6'>Jun</MenuItem>
                     <MenuItem value='7'>July</MenuItem>
                     <MenuItem value='8'>Aug</MenuItem>
                     <MenuItem value='9'>Sep</MenuItem>
                     <MenuItem value='10'>Oct</MenuItem>
                     <MenuItem value='11'>Nov</MenuItem>
                     <MenuItem value='12'>Dec</MenuItem>
                </Select>
                <Select
                    labelId="year"
                    id="demo-simple-select-placeholder-label"
                    displayEmpty
                    className={classes.date}
                    value={this.props.values.year}
                    onChange={this.props.handleChange('year')}
                    >
                        <MenuItem value="" disabled>
                            Year
                        </MenuItem>
                     {this.renderYear()}
                </Select>

            
                </center>
                </Grid>
                <Grid item xs={null} sm={3} />
            </Grid>
            <Box component='div' m={5}>
            <center>
            {this.props.values.day !== '' && this.props.values.month !== '' && this.props.values.year !== '' &&
            <Link to='/labelSelection'><Button variant="contained" >Start</Button></Link>}
             {(this.props.values.day === '' || this.props.values.month === '' || this.props.values.year === '') &&
                <Button variant="contained" disabled>Start</Button>}

            </center>
            </Box>
            </Box>

            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(StandByPage);