import React, { Component } from 'react';
import Logo from './logo';
import {Grid, Box, withStyles, Button, Card, CardMedia, CardActionArea, TextField} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { getBottles } from '../services/bottleService';
import imageSample from '../images/bottle1.png';

const style = theme => ({
    media: {
        height: 600
      },
    textfield: {
        width: '100%'
    },
    button: {
        margin: 15,
        fontSize: 15

    },
    background: {

        height: 700,
        width: '100%',
        backgroundImage: `url(${imageSample})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        border: 'solid red 1px',
    },
    text: {
        marginTop: '98%',  //400
        marginLeft: '30%',   //120
        fontSize: 55,
        width: 170,
        background: 'white',
        fontStyle: 'italic',
        fontFamily: 'Arial',
    }


})

class Customize extends Component {
    state = { 
       bottles: [],
     }


    populateBottles = async () => {
        const {data: bottles} = await getBottles();
        this.setState({bottles});
        }

    async componentDidMount() {
        await this.populateBottles();
    }
    
    render() { 
        const {classes, handleChange, values} = this.props;
        //values.bottleId = this.props.match.params.id;
        const id = values.bottleId;
        const {bottles} = this.state;

        function filterBottles(bottles, id){
            const filtered = bottles.filter(f => f.id === id);
            return filtered;
        }

        const filtered = filterBottles(bottles, id);
        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
             Customize Your label
             
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                {filtered.map(bottle => {
                    return (
                <Grid key={bottle.id} item xs={12} sm={3}>
                <Card className={classes.card}>
                    {/* <Box component='div' className={classes.background} >
                    <Box component='div' className={classes.text} > */}
                    {values.labelText}
                    {/* </Box>
                    </Box> */}
                    <CardActionArea>
                        <CardMedia
                        className={classes.media}
                        image={bottle.image}
                        />
                    </CardActionArea>
                </Card>
                </Grid>
                    )
                })}
                <Grid item xs={null} sm={2} />
                <Grid item xs={12} sm={5}>
                <TextField className={classes.textfield} id="outlined-basic" onChange={handleChange('labelText')} value={values.labelText} label="Enter label here" variant="outlined" required/>
                </Grid>
                <Grid item xs={null} sm={2} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <center>
            <Link to='/labelSelection' ><Button className={classes.button} variant="contained">Back</Button></Link>
            {values.labelText === '' && 
            <Button variant="contained" disabled>Next</Button>}
            {values.labelText !== '' && 
            <Link to='/dataCollection'><Button className={classes.button} variant="contained" >Next</Button></Link>}
            </center>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(Customize);