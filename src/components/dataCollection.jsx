import React, { Component } from 'react';
import Logo from './logo';
import {Link} from 'react-router-dom';
//import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator';
import validator from 'validator';
import {Grid, Box, withStyles, Button, TextField, } from '@material-ui/core';

const style = theme => ({
    border: {
    border: 'red solid 2px',
    },
    date: {
        justifyItem: 'center'
    },
    media: {
        height: 600
      },
    textfield: {
        width: '100%',
        margin: '10px'
    },
    button: {
        margin: 15,
        fontSize: 15

    }

})

class DataCollection extends Component {
    state = { 
        check : false
     }

    
    checkEmail = () => {
        if(!validator.isEmail(this.props.values.email))
        return 'Enter valid Email'
    }

    
    render() { 
        const {classes, handleChange, values} = this.props;
        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
            Fill out your information
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                <Grid item xs={null} sm={3} />
                <Grid item xs={12} sm={6}>
                <TextField className={classes.textfield} onChange={handleChange('firstname')} value={values.firstname} id="outlined-basic" label="First name" variant="outlined" required/>
                <TextField className={classes.textfield} onChange={handleChange('lastname')} value={values.lastname} id="outlined-basic" label="Last name" variant="outlined" required/>
                <TextField helperText={(this.state.check)? this.checkEmail() : ''} error={(this.state.check) ? !validator.isEmail(values.email) : false} onClick={() => this.setState({check: true})}  className={classes.textfield}  onChange={handleChange('email')} value={values.email} id="outlined-basic" label="Email Address" variant="outlined" type='email' required/>
                <TextField className={classes.textfield} onChange={handleChange('zipcode')} value={values.zipcode} id="outlined-basic" label="Zip code" variant="outlined" required/>
                </Grid>
                <Grid item xs={null} sm={3} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <center>
            <Link to={'/customize'}><Button className={classes.button} variant="contained">Back</Button></Link>
            {values.firstname !== '' && values.lastname !== '' && validator.isEmail(values.email) && values.zipcode !== '' && 
            <Link to='/survey/1'><Button className={classes.button} variant="contained" type="submit">Next</Button></Link>}
            {(values.firstname === '' || values.lastname === '' || !validator.isEmail(values.email) || values.zipcode === '') && 
            <Button variant="contained" disabled>Next</Button>}
            </center>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(DataCollection);