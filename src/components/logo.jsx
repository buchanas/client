import React, {Component} from 'react';
import { Avatar, Typography, Grid, AppBar, Toolbar, Button, withStyles} from '@material-ui/core';
import logo from '../images/companylogo.jpg';
import auth from '../services/authService';
import {Link} from 'react-router-dom';
import firebase from 'firebase';

 
const styles = theme => ({
    logo: {
        width: theme.spacing(8),
        height: theme.spacing(8),
    },
    brandName: {
        justify: 'right'
    },
    button: {
        margin: 15,
    }
})




class Logo extends Component {
    state= {
        user: {},
    }

    componentDidMount = () => {
        const user = auth.getCurrentUser();
        this.setState({user});
    }
    
    handleClick = () => {
        firebase.auth().signOut()
                .then(function() {
                    sessionStorage.clear();
                    localStorage.clear();
                    window.location.assign('https://accounts.google.com/Logout');
                    window.location = '/';
                })
                .catch(function(error) {
                    console.log(error);
                });
    }
    

    render (){
    const {user} = this.state;
    const {classes} = this.props;
    

    return ( 
        <AppBar position='static'>
        
        <Toolbar>
        <Grid container>
        <Grid item xs={1}>
        <Avatar className={classes.logo} alt="logo" src={logo} />
        </Grid>
        <Grid item xs={10}>
        <Typography className={classes.brandName} variant='h2' >
            Brand Name
        </Typography>
        </Grid>
        <Grid item xs={1}>
        {user === null && 
        <Link to='/login'><Button className={classes.button} primary='true'  variant="contained" >login</Button></Link>}
        {user !== null && <><Button className={classes.button} primary='true' onClick={this.handleClick}  variant="contained" >logout</Button></>}
        </Grid>
        </Grid>
        </Toolbar>
        </AppBar>
     );}
}
 
export default withStyles(styles)(Logo);