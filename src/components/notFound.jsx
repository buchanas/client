import React from 'react';

const notFound = () => {
    return (
        <h2> Oops Page Not Found!!</h2>
    );
}

export default notFound;