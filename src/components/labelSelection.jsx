import React, { Component } from 'react';
import Logo from './logo';
import {Grid, Box, withStyles, Button, Card, CardMedia, CardActionArea, CardContent, Typography} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { getBottles } from '../services/bottleService';

const style = theme => ({

    media: {
        height: 600
      },
    cardAction: {
        border: 'solid red 10px'
    },
    button: {
        margin: 15,
        fontSize: 15

    }

})

class LabelSelection extends Component {
    state = { 
        bottles: [],
     }

    populateBottles = async () => {
        const {data: bottles}  = await getBottles();
        this.setState({bottles});
    }

    async componentDidMount() {
        await this.populateBottles();
    }

   
    
    render() { 
        const {classes, handleClick, values} = this.props;
        function getClass(id){
           if(values.bottleId === id)
            return (
                classes.cardAction
            )

            return null
            
        }
        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
             Choose your occasion
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container spacing={5}>
                {this.state.bottles.map(bottle => {
                return (
                <Grid key={bottle.id} item xs={12} sm={3}>
                <Card onClick={() => {handleClick(bottle.id);}} className={getClass(bottle.id)}>
                        <CardActionArea>
                        <CardMedia
                        className={classes.media}
                        image={`${bottle.image}`}
                        />
                    </CardActionArea>
                    <CardContent className={classes.title}>
                        <Typography gutterBottom variant="h5" component="h2">
                            {bottle.title}
                        </Typography>
                    </CardContent>
                </Card> 
                </Grid>
                )
                })}
                </Grid>
            </Box>
            <Box component='div' m={5}>
            <center>
            <Link to='/customize' ><Button className={classes.button} variant="contained">next</Button></Link>
            </center>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(LabelSelection);