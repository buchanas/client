import React, { Component } from 'react';
import Logo from './logo';
import {getQuestion, saveQuestion} from '../services/questionService';
import {Grid, Box, withStyles, Button, TextField} from '@material-ui/core';
import {ValidatorForm} from 'react-material-ui-form-validator';
import { Link } from 'react-router-dom';

const style = theme => ({
      textfield: {
        width: '100%',
        margin: '10px'
    },
    button: {
        margin: 15,
        fontSize: 15

    }

})

class QuestionEdit extends Component {
    state = { 
        question: {
            id: '',
            title: '',
            options: []
        },
        oldTitle: '',
    }

    populateQuestion = async () => {
        try {
            const questionId = this.props.match.params.id;
            const { data: question } = await getQuestion(questionId);
            const id = this.props.match.params.id;
            this.setState({oldTitle: question.title});
            this.setState({question: this.mapToviewModel(question, id)});
            
          } catch (ex) {
            if (ex.response && ex.response.status === 404)
              this.props.history.replace("/notFound");
          }
        }

    mapToviewModel(question, id) {
    return {
      id: id,
      title: question.title,
      options: question.options
    }; 
    }


    handleChange = event => {
        const question = {...this.state.question};
        question[event.target.name] = event.target.value;
        this.setState({question})
    }

    handleChangeOption = index => event => {
        const question = {...this.state.question};
        const options = [...this.state.question.options];
        options[index] = event.target.value;
        question.options = options;
        this.setState({question});
    }


    handleSubmit = async () => {
        saveQuestion(this.state.question)
        .then(res => this.props.history.replace('/admin/questions'))
        .catch(err => alert(err) );
    }
    


    async componentDidMount() {
        await this.populateQuestion();
    }
    
    render() { 
        const {classes} = this.props;
        const {question, oldTitle} = this.state;


        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
            Edit Questions
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                <Grid item xs={null} sm={3} />
                <Grid item xs={12} sm={6}>
                <ValidatorForm onSubmit={this.handleSubmit} >
                <TextField value={question.title} name='title' onChange={this.handleChange} className={classes.textfield} id="outlined-basic" label={oldTitle} variant="outlined"/>
                {question.options.map((option, index) => {
                    return <TextField key={index} value={option} onChange={this.handleChangeOption(index)} className={classes.textfield} label={`option ${index + 1}`} id="outlined-basic" variant="outlined"/>
                })}
                <center>
                <Link to='/admin/questions'><Button className={classes.button} variant="contained" >Back</Button></Link>
                <Button className={classes.button} variant="contained" type='submit'>Save</Button>
                </center>
                </ValidatorForm>
                </Grid>
                <Grid item xs={null} sm={3} />
            </Grid>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(QuestionEdit);