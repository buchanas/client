import React, { Component } from 'react';
import Logo from './logo';
import {Link} from 'react-router-dom';
import {getQuestions} from '../services/questionService';
import {Grid, Box, withStyles, Button, TextField,} from '@material-ui/core';

const style = theme => ({
      textfield: {
        width: '100%',
        margin: '10px'
    }

})

class Questions extends Component {
    state = { 
        questions: [],
    }

    populateQuestion = async () => {
        const {data: questions} = await getQuestions();
        this.setState({questions});
    }

    async componentDidMount() {
        await this.populateQuestion();
    }
    
    render() { 
        const {classes} = this.props;
        const {questions} = this.state;
        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
            All Questions!!
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                <Grid item xs={null} sm={3} />
                <Grid item xs={12} sm={6}>
                {questions.map(question => {
                    return (
                    <Grid key={question.id} container>
                    <Grid item xs={9}>
                    <TextField key={question.id} className={classes.textfield} id="outlined-basic" label={question.title} variant="outlined" disabled/>
                    </Grid>
                    <Grid item xs={1} />
                    <Grid item xs={2}>
                    <Link to={`/admin/questions/${question.id}`}><Button variant="contained">Edit</Button></Link>
                    </Grid>
                    </Grid>
                    )
                })}
                </Grid>
                <Grid item xs={null} sm={3} />
            </Grid>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(Questions);