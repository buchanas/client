import React, { Component } from 'react';
import Logo from './logo';
import {Grid, Box, withStyles, Button,} from '@material-ui/core';

const style = theme => ({
 
})

class StartOver extends Component {
    state = {  }

    
    handleClick = () => {
        window.location ='/';
    }

    timeOut = () => {
        setTimeout(
            this.handleClick
        , 15000)
    }
    componentDidMount = () => {
       this.timeOut();
    }
    
    render() { 
       // const {classes} = this.props;

        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                <Grid item xs={0} sm={2} />
                <Grid item xs={0} sm={8} >
                <center>
                    <h2>
                    Your Label is now Printing. Please Collect it from ambassador! 
                    <br />
                    You will be redirect to homepage within 15 seconds
                    </h2> 
                </center>
                </Grid>
                <Grid item xs={0} sm={2} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <center>
            <Button onClick={this.handleClick} variant="contained">Start over</Button>
            </center>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(StartOver);