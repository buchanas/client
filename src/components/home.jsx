import React, { Component } from 'react';
import LabelSelection from './labelSelection';
import Customize from './customize';
import DataCollection from './dataCollection';
import Survey from './survey';
import Review from './review';
import StartOver from './startover';
import StandByPage from './standbypage';
import {Route, Switch, Redirect} from 'react-router-dom';
import QuestionEdit from './questionEdit';
import Login from './login';
import Questions from './questions';
import ProtectedRoute from './protectedRoute';
import NotFound from './notFound';


class Home extends Component {
    state = { 
        day: '',
        month: '',
        year: '',
        bottleId: '1',
        labelText: '',
        firstname: '',
        lastname: '',
        email: '',
        zipcode: '',
        answer: [],
     }

     handleChange = input => event => {

        this.setState({[input]: event.target.value});
    }
    
    handleChangeImage = (input, id) => event => {
        this.setState({[input]: event.target.value});
        this.setState({bottleId: id});
    }

    handleClick = id => {
        this.setState({bottleId: id});
    }

    handleClickOption = (option, questionId) => {
        const answer = [...this.state.answer];
        answer[questionId] = option;

        this.setState({answer});
    }


    render() { 
         const { day, month, year, date, bottleId, labelText, firstname, lastname, email, zipcode, answer} = this.state;
         const values = { day, month, year, date, bottleId, labelText, firstname, lastname, email, zipcode, answer};
        return ( 
            <Switch>
                <Route path="/labelSelection" render={props => <LabelSelection {...props} handleClick={this.handleClick} values={values}  />}   />
                <Route path="/customize" render={props => <Customize {...props} handleChange={this.handleChange} values={values}  />} />
                <Route path="/dataCollection" render={props => <DataCollection {...props} handleChange={this.handleChange} values={values}  />} />
                <Route path="/survey/:id" render={props => <Survey {...props} handleClick={this.handleClickOption} values={values}  />} />
                <Route path="/review" render={props => <Review {...props} handleChange={this.handleChange} values={values}  />} />
                <Route path="/startover" component={StartOver} />
                <Route path="/login" component={Login} />
                <Route path="/notFound" component={NotFound} />
                <ProtectedRoute path="/admin/questions/:id" component={QuestionEdit} />
                <ProtectedRoute path="/admin/questions" component={Questions} />
                <Route exact path="/" render={props => <StandByPage {...props} handleChange={this.handleChange} values={values}  />} />
                <Redirect to='/notFound' />
            </Switch>
         );
    }
}
 
export default Home;