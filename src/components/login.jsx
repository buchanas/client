import React , {Component} from 'react';
import {Box, Grid, Typography, Button, Container, AppBar } from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import NavBar from "./logo.jsx";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Link } from 'react-router-dom';
// import auth from "../services/authService";
import firebase from 'firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

const firebaseConfig = {
    apiKey: "AIzaSyAh_8DZuDRej8vCaLCLd9HEv5FIEWwY8Co",
    authDomain: "first-firebase-project-392fe.firebaseapp.com",
    databaseURL: "https://first-firebase-project-392fe.firebaseio.com",
    projectId: "first-firebase-project-392fe",
    storageBucket: "first-firebase-project-392fe.appspot.com",
    messagingSenderId: "279595208066",
    appId: "1:279595208066:web:f141c611f61e9dcb97539b",
    measurementId: "G-M83RZ58RB9"
  };
  
firebase.initializeApp(firebaseConfig);






const useStyles = theme => ({
    formContainer:{
            
        border: 'solid black 1px',
        borderRadius: 20,
        textAlign: "center",
        padding: 30,
    },
    TextValidator:{
        marginBottom: 10, 
        width: "100%"
    },
    button:{
        width: "100%"
    },
    appBar: {
       
    },
})

class Login extends Component {
    
    state ={
        formData: {
            email: "",
            password: ""        },
        submitted: false,
        isSignedIn: false
    }

    uiConfig = {
        signInFlow: 'popup',
        signInOptions: [
        {
            provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            scopes: [
              'https://www.googleapis.com/auth/plus.login'
            ],
            customParameters: {
              // Forces account selection even when one account
              // is available.
              prompt: 'select_account'
            }
          }],
            //firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
            // firebase.auth.GithubAuthProvider.PROVIDER_ID,
        callbacks: {
            signInSuccess: () => {
                window.location= "/admin/questions";
                const data = [localStorage.getItem('firebaseui::rememberedAccounts')];
                const username = data[0];
             localStorage.setItem('username', username)}
        }
    }

   

    componentDidMount = () => {
       
        firebase.auth().onAuthStateChanged(user => {
            this.setState({ isSignedIn: !!user });
        })
    }

    handleSubmit = async() => {
        
        const { email: identifier, password } = this.state.formData;
        
      await firebase.auth().signInWithEmailAndPassword(identifier, password).then(response => {
        //if(localStorage.getItem('role') === 'admin')
        //localStorage.setItem('token', response.user.stsTokenManager.accessToken); // JWT= JSON Web Token
        localStorage.setItem('username', response.user.email);
        window.location= "/admin/questions";
        // if(localStorage.getItem('role') === 'Authenticated')
        //     window.location= "/";
        })
        .catch(error => { 
        // if (error.response.status === 400) {
            // .response.data.data[0].messages[0].message
          alert(error);
        
    })
    }

    render() {
        const {classes} = this.props;

    

    


    const handleChange = event => {
        const {formData} = {...this.state};
        formData[event.target.name] = event.target.value;
        this.setState({formData});
    }


    return ( 
        <>
            <NavBar />
            <Box my={10} >
            <Container maxWidth="sm" scroll="body">
            <Grid className={classes.formContainer} container justify="center">
                <Grid item >
                <Typography variant="h4">
                 <AppBar className={classes.appBar} position="static">    LOGIN HERE </AppBar>
                </Typography>
                <ValidatorForm onSubmit={this.handleSubmit} >
                <Box my={2}>
                <TextValidator onChange={handleChange} name="email" className={classes.TextValidator} id="outlined-basic" validators={["required","isEmail"]} errorMessages={["this feild is required","Not a valid Email"]}  label="Email" value={this.state.formData.email} variant="outlined" />
                <br />
                <TextValidator onChange={handleChange} name="password" className={classes.TextValidator} id="outlined-basic" validators={["required"]} errorMessages={["tthis feild is required"]}  label="Password" value={this.state.formData.password} variant="outlined" type="password" />
                </Box>
                <Button className={classes.button} variant="outlined" color="secondary" type="submit">
                 Login
                </Button>
                <StyledFirebaseAuth uiCallback={ui => ui.disableAutoSignIn()} uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
                </ValidatorForm>
                </Grid>
            </Grid>
            </Container>
            </Box>
            <Box component='div' m={5}>
            <center>
            
            <Link to='/'><Button variant="contained">Home</Button></Link>

            </center>
            </Box>
          </>  
        
     );
    }
}
 
export default withStyles(useStyles)(Login);