import React, { Component } from 'react';
import Logo from './logo';
import {Link} from 'react-router-dom';
import {getQuestions} from '../services/questionService';
import {Grid, Box, withStyles, Button, Typography,} from '@material-ui/core';

const style = theme => ({
      textfield: {
        width: '100%',
        margin: '0px',
        marginBottom: 50,
        border: 'solid 1px black',
        borderRadius: 10,
        padding: 20,
         
    },
    button: {
        margin: 15,
        fontSize: 15

    },
    optionSelected: {
        width: '100%',
        margin: 20,
        fontSize: 20,
        border: 'solid red 2px'
    },
    option: {
        width: '100%',
        margin: 20,
        marginBottom: 10,
        fontSize: 20,
    }

})



class Survey extends Component {
    state = { 
        questions: [],
    }

    populateQuestion = async () => {
        const {data: questions} = await getQuestions();
        this.setState({questions});
    }

    async componentDidMount() {
        await this.populateQuestion();
    }
    
    componentDidUpdate() {
        this.checkFilter(this.state.questions);
    }

    checkFilter = questions => {
        if(questions.length < this.props.match.params.id){ this.props.history.replace('/review')};

    }

    
    render() { 
        const {classes, values, handleClick} = this.props;
        const id = this.props.match.params.id;
        const {questions} = this.state;
        const next = Number(id) + 1;
        const back = Number(id) - 1;
        if(Number(id) === 0){ this.props.history.replace('/dataCollection')}
        function filterQuestions(questions, id){
            const filtered = questions.filter(f => f.id === id);
            return filtered;
        }
        
        const filtered = filterQuestions(questions, id);



        function getClass(op, qId){
            if(values.answer[qId] === op)
             return (
                 
                 classes.optionSelected
             )
 
             return (
                 classes.option
             )
             
         }
       
        return (  
            <React.Fragment>
            <Box component='div' m={0}>
            <Grid container >
                <Grid item xs={null} sm={null} />
                <Grid item xs={12} sm={12} >
                    <Logo />
                </Grid>
            <Grid item xs={null} sm={null} />
            </Grid>
            </Box>
            <center>
            <h2>
            Survey Questions?
            </h2> 
            </center>
            <Box component='div' m={5}>
            <Grid container alignItems='center' spacing={5}>
                <Grid item xs={null} sm={2} />
                <Grid item xs={12} sm={8}>
                {filtered.map(question => {
                    this.checkFilter(question);
                    return ( <div key={question.id}> <Typography variant='h5'  className={classes.textfield} id="outlined-basic" >{question.title}</Typography>
                    {question.options.map((option, i) => { 
                        return (
                        <div key={i}>
                        <Grid  container>
                        <Button onClick={() => handleClick(option, question.id)} className={getClass(option, question.id)} variant="outlined">{option}</Button>
                        <br />
                        </Grid>
                        </div>) 
                        })}
                       </div> )

                    })}
                </Grid>
                {/* <Grid item xs={12} sm={5}>
                <TextField className={classes.textfield} onChange={handleChange('answer1')} value={values.answer1} id="outlined-basic" label="Answer 1"  />
                <TextField className={classes.textfield} onChange={handleChange('answer2')} value={values.answer2} id="outlined-basic" label="Answer 2"  />
                <TextField className={classes.textfield} onChange={handleChange('answer3')} value={values.answer3} id="outlined-basic" label="Answer 3"  />
                <TextField className={classes.textfield} onChange={handleChange('answer4')} value={values.answer4} id="outlined-basic" label="Answer 4"  />
                </Grid> */}
                <Grid item xs={null} sm={2} />
            </Grid>
            </Box>
            <Box component='div' m={5}>
            <center>
            <Link to={`/survey/${back}`}><Button className={classes.button} variant="contained">Back</Button></Link>
            <Link to={`/survey/${next}`}><Button className={classes.button} variant="contained">next</Button></Link>


            {/* {values.awnser1 !== '' && values.answer2 !== '' && values.answer3 !== '' && values.answer4 !== '' && 
            <Link to='/review'><Button className={classes.button} variant="contained" type="submit">Next</Button></Link>}
            {(values.awnser1 === '' || values.answer2 === '' || values.answer3 === '' || values.answer4 === '') && 
            <Button variant="contained" disabled>Next</Button>} */}
            </center>
            </Box>
            </React.Fragment>
        );
    }
}
 
export default withStyles(style)(Survey);